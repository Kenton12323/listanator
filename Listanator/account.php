 <?php 
	    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
		<title>Account Management</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
    </head>

    <body>
		<?php include 'header.php'; 
			include 'secure.php';
			if( !loggedIn()){
				header('Location: login.php');
			}
		?>
		<article>
		    <h2>
		    Account Management
		    </h2>
		    <p> 
			    This page allows you to change your username, password or delete your account if you so wish (please don't!)
		    </p>
		    <h3>
			    Change password
		    </h3>
		    <form>
			    <p>
			    Enter your current password here:<p>
			    <input type='password' name='current_password'> 

			    <p> Enter your new password here: </p>
			    <input type='password' name='new_password_1'>

			    <p> Confirm your new password here: </p>
			    <input type='password' name='new_password_2'>
			    <input type='submit' value='Change Password'>

		    </form>
		    <h3> Change username </h3>
		    <form>
			    <p> Enter your new username here: </p>
			    <input name="username">

			    <input type='submit' value='Change Username'>
		    <h3> Delete account </h3>
		    </form>
			    
			    <form>
			    Click here to delete your account if you really want to - we'd much rather you stay! <br>
			    <input type='submit' value='Delete my account'>
		    </form>
	    </article>
	  <?php include 'footer.php' ?> 
    </body>
</html>
