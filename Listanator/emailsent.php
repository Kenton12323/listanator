 <?php 
	    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
		<title>Emal sent</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
    </head>

    <body>
		<?php include 'header.php'; ?>
		<article>
		    <h2>
			    Email sent
		    </h2>
		    
		    <p>
		    An email has been sent to your account - please click the link in the email to confirm your new password!
		    </p>
	    </article>
	<?php include 'footer.php' ?> 
    </body>
</html>