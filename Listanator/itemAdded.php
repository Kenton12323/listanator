<?php  

   include 'header.php' ;
   include 'secure.php';
	if(!loggedIn()){
		header('Location: login.php');
	}
    include 'database.php' ;
    $db = new Database;
   
   
	if(isset($_POST['itemBody']) && isset($_POST['list'])){

	    $stmt = $db->prepare("INSERT INTO items (listID,body,position,checked) VALUES (:id,:body,:position,:checked)");
	    $stmt->bindValue(':id', $_POST['list'], SQLITE3_INTEGER );
	    $stmt->bindValue(':body', $_POST['itemBody'], SQLITE3_TEXT);
	    $stmt->bindValue(':position', '1', SQLITE3_INTEGER );
	    $stmt->bindValue(':checked', '0', SQLITE3_INTEGER );
	    
	    $results = $stmt->execute();
	}
    header('Location: listview.php?list_id='.$_POST['list']);
?>
