 <?php 
	    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
		<title>CS139 Listanator List</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script src='js/jquery-3.1.1.min.js'></script>
		<script src='js/listview.js'></script>
    </head>

    <body>
	   <?php 
	  
		include 'header.php' ;
		include 'secure.php';
			if( !loggedIn()){
				header('Location: login.php');
			}
		include 'database.php' ;
		$db = new Database;
		
	   ?> 
	    <article>
		    <h2>
			<?php
			    $stmt = $db->prepare("SELECT * FROM users WHERE \"ID\"=:id");
			    $stmt->bindValue(':id', $_SESSION['id'], SQLITE3_INTEGER);
			    $results = $stmt->execute();
			    
			    $row = $results->fetchArray();
			    
			    echo h($row['title'])." ".h($row['lastname']). "'s Lists";
			?>
		    </h2>
		    <ul id="list">
		    <?php
			if(isset($_GET['list_id'])){
			
				$stmt = $db->prepare("SELECT * FROM lists WHERE \"ID\"=:id");
				$stmt->bindValue(':id', $_GET['list_id'], SQLITE3_INTEGER); 
				
				$results = $stmt->execute();
				$row = $results->fetchArray();
				
				if(accessResource($row['ownerID'])){
				

					$stmt = $db->prepare("SELECT * FROM items WHERE \"listID\"=:id");
					$stmt->bindValue(':id', $_GET['list_id'], SQLITE3_INTEGER);
					$results = $stmt->execute();
					
					while($row = $results->fetchArray()){
					    if($row['checked'] == 1){
						echo "<li data-id=".$row['ID'].">".h($row['body'])." <button class=\"check\"> Complete</button><input id=\"checker_".$row['ID']."\" type=\"checkbox\" disabled checked><br></li>";
					    }else {
						echo "<li data-id=".$row['ID'].">".h($row['body'])." <button class=\"check\"> Complete</button><input class=\"checker_".$row['ID']."\" type=\"checkbox\" disabled><br></li>";
					    }
						
					}
					echo "</ul>";
					
					echo "<form method=\"post\" accept-charset=\"utf-8\" id=\"item_form\" data-list_id=".$_GET['list_id'].">";
					
					?>
					    <label>Item name</label><br><input id='name' name="name"><br>
					    <input type='submit' value='Add Item'><br>	
					</form>
					<?php
				}
				else {
					echo "You may not access this list";
				}
				echo "<a href=\"listview.php\">Back to see all lists.</a>";
			}else{  
			
			$stmt = $db->prepare("SELECT * FROM lists WHERE \"ownerID\"=:id");
			$stmt->bindValue(':id', $_SESSION['id'], SQLITE3_INTEGER);
			$results = $stmt->execute();
						
			while($row = $results->fetchArray()){
			    echo "<a href=\"listview.php?list_id=".h($row['ID'])."\">".h($row['name'])."</a><br>";
			}
			
			echo "<a href=\"addList.php\">Add New List</a>";
			}			
		    ?> 
			    
	    </article>
	    <?php include 'footer.php' ?> 
    </body>
</html>
