 <?php 
	    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
	<title>CS139 Listanator </title>
	<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
    </head>

    <body>
	
	    <?php include 'header.php'; 
			include 'secure.php';
			if( !loggedIn()){
				header('Location: login.php');
			}
		?>
		<article>
		<h2>
			Add a new list item
		</h2>
		
		<p>
		    Here you can add a new item to your collection.
		</p>
		
			<form action="itemAdded.php" method="post"> 
		    <label>What is this list item:</label><input type="text" name="itemBody">
			<input type="hidden" name="list" value="<?php echo $_GET['list_id']?>">
		    <input type="submit" value="Submit">
		</form>
		<br>
		</article>
	<?php include "footer.php" ?>
    </body>
</html>
