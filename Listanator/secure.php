<?php 
	session_start();
	
	function loggedIn() {
		return (isset($_SESSION['id']));
	}

	function accessResource($resource_user_id){
		return ($_SESSION['id'] == $resource_user_id);
	}
	
	function h($output_string){
		return  htmlspecialchars($output_string, ENT_QUOTES, 'utf-8');
	}
?>

