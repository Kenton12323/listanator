PRAGMA foreign_keys = ON;

INSERT INTO users (firstname,lastname,title,username,email,password,salt) VALUES ("Me", "Myself", "Mr", "AndI", "me@myself.andI", "password","finely ground");

INSERT INTO lists (ownerID,name,created,lastEdited) VALUES ("11", "Test List 1", "01/03/1922", "01/03/1928 05:14:20");

INSERT INTO items (listID,body,position,checked) VALUES ("7", "Many good things", "1", "0");
INSERT INTO items (listID,body,position,checked) VALUES ("7", "Many good things", "2", "0");
INSERT INTO items (listID,body,position,checked) VALUES ("7", "Many good things", "3", "0");
INSERT INTO items (listID,body,position,checked) VALUES ("7", "Dom the Narcissist", "3", "0");