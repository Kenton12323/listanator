 <?php 
	    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
		<title>Log in</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
    </head>

    <body>
		<?php include 'header.php'; ?>
		<article>
		    <h2>
		    Log-in
		    </h2>
		    <h3>
			    Please enter your log-indetails below
		    </h3>
		     <form action='loginCheck.php' method="post">
			    <label>Username</label><br><input name="username"><br>
			    <label>Password</label><br><input type='password' name='password'><br>
			    <input type='submit' value='Log in'><br>	
			    <a href="forgot.html">I have forgot my password</a>
		    </form>
		</article>
	<?php include 'footer.php' ?> 
    </body>
</html>
