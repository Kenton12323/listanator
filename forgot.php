 <?php 
	    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
		<title>Forgot my password</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
    </head>

    <body>
		<?php include 'header.php'; ?>
		<article>
		    <h2>
			Forgot my password
		    </h2> 
		    <p> 
			    This page allows you to generate a new password if you have forgotten yours.
		    </p>
		    <h3>
			    Make me a new password
		    </h3>
		    <p>
			    Enter your email address here - we will email you a confirmation email with your new password, as well as a link to confirm it.
		    </p>
		    <form>
			    <input name='username' value='user_email'> 
			    <input type='submit' value='Create new password'>
		    </form>
		</article>
		
		<?php include 'footer.php' ?> 
		
	</body>
</html>
