 <?php 
	    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
	<title>CS139 Listanator </title>
	<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
    </head>

    <body>
	
	    <?php include 'header.php'; 
			include 'secure.php';
			if( !loggedIn()){
				header('Location: login.php');
			}
		?>
		<article>
		<h2>
			Add a new list
		</h2>
		
		<p>
		    Here you can add a new list to your collection.
		</p>
		<form action="listAdded.php" method="post"> 
		    <label>Name of new list:</label><input type="text" name="listName">
		    <input type="submit" value="Submit">
		</form>
		<br>
		</article>
	<?php include "footer.php" ?>
    </body>
</html>
