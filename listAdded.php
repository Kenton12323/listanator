<?php  

   include 'header.php' ;
   include 'secure.php';
	if(!loggedIn()){
		header('Location: login.php');
	}
    include 'database.php' ;
    $db = new Database;
   
   
	if(isset($_POST['listName'])){
	    
	    $stmt = $db->prepare("INSERT INTO lists (ownerID,name,created,lastEdited) VALUES (:id,:listName,:date,:time)");
	    $stmt->bindValue(':id', $_SESSION['id'], SQLITE3_INTEGER );
	    $stmt->bindValue(':listName', $_POST['listName'], SQLITE3_TEXT);
	    $stmt->bindValue(':date', date("d/m/Y"), SQLITE3_TEXT );
	    $stmt->bindValue(':time', date("d/m/Y")." ".date("H:i:s"), SQLITE3_TEXT );
	    
	    $results = $stmt->execute();
	  
	}
    header('Location: listview.php');
?>
