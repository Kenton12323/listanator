

DROP TABLE users;
CREATE TABLE users (
    ID INTEGER PRIMARY KEY AUTOINCREMENT, 
    firstname varchar(20),
    lastname varchar(20),
    title varchar(10), 
    username varchar(30),
    email varchar(50), 
    password varchar(40),
    salt varchar(40)
);

DROP TABLE lists;
    CREATE TABLE lists (
    ID INTEGER PRIMARY KEY AUTOINCREMENT, 
    ownerID integer, 
    name varchar(50), 
    created varchar(10), 
    lastEdited varchar(19) , 
    FOREIGN KEY(ownerID) REFERENCES users(ID)
);

    
DROP TABLE items;
CREATE TABLE items (ID INTEGER PRIMARY KEY AUTOINCREMENT, listID integer,  body varchar(100), position integer, checked integer, 
    FOREIGN KEY(listID) REFERENCES lists(ID)
);
   
 